import styled from "styled-components";

export const SliderContainer = styled.div`
	position: relative;
	margin: 4em auto;
	width: 70%;
	height: 570px;
	border: 1px solid #000;
`;

export const Figure = styled.figure`
	display: flex;
	width: 100%;
	height: 100%;
`;

export const Img = styled.img`
	width: 100%;
	height: 100%;
	object-fit: cover;
`;
